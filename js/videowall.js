// THREE.JS BASE

var extraSize = 0,
	SPEEDX    = .001,
	SPEEDY    = .001,
	SPEEDZ    = .001,
	DISTANCE  = 500,
	CAM_DIST  = 5000;

var T ={
	canvasWidth        : 1920,
	canvasHeight       : 1080,
	frame              : 0,
	radius             : 2000,
	extraParticles     : 1500,
	decreaseParticlesBy: 1,
	particleNoiseSpeed : 0.02,
	mainColour         : 0x0f9be6,
	textureLink        : 'img/head-black.png',//'/templates/aopen_webgl_videowall/img/c2.png', //
	rendererBackground : 0xffffff,
	cameraDistance     : 4500,
	particleSizing     : function(){ return 10 + Math.random()*90; },
	
	init: function(){
		T.container = document.createElement( 'div' );
		$('#three_js').children('.inner').first().append(T.container);

		T.scene = new THREE.Scene();

		T.camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 1, 10000 );
		T.camera.position.z = T.cameraDistance;
		T.scene.add( T.camera );
		
		T.configureParticles();
		T.addParticles();
		
		T.appendRenderer();
		
		T.Animations.bringInParticles();
		
		T.animate();
		
		T.bindEvents();
	},
	
	appendRenderer: function(){
		T.renderer = new THREE.WebGLRenderer({antialias: true});
		T.renderer.setClearColorHex( T.rendererBackground, 1 );
		// T.renderer.setSize( window.innerWidth, window.innerHeight );
		T.renderer.setSize( T.canvasWidth, T.canvasHeight );
		T.renderer.autoClear = true;
		// THREEx.WindowResize(T.renderer, T.camera);
	
		T.container.appendChild( T.renderer.domElement );
	},
	
	bindEvents: function(){
	},
	
	configureParticles: function(){
		// particles
		T.map = THREE.ImageUtils.loadTexture( T.textureLink );
		
		T.attributes = {
			size			: {	type: 'f', value: [] },
			custompositiona : { type: 'v3', value: [] },
			custompositionb : { type: 'v3', value: [] },
			custompositionc : { type: 'v3', value: [] },
			custompositiond : { type: 'v3', value: [] },
			custompositione : { type: 'v3', value: [] },
			custompositionf : { type: 'v3', value: [] },
			custompositiong : { type: 'v3', value: [] },
			custompositionh : { type: 'v3', value: [] },
			custompositioni : { type: 'v3', value: [] },
			custompositionj : { type: 'v3', value: [] },
			customColor		: { type: 'c', value: [] },
			customTexture   : { type: 't', value: [] }
		};

		T.uniforms = {
			amplitude : { type: "f", value: 0},
			color	  : { type: "c", value: new THREE.Color( T.mainColour ) },
			texture	  : { type: "t", value: 0, texture: T.map },
			time	  : { type: "f", value: 1.0 },
			direction : { type: "f", value: 1.0 },
		};

		T.shaderMaterial = new THREE.ShaderMaterial( {

			uniforms	   : T.uniforms,
			attributes	   : T.attributes,
			vertexShader   : document.getElementById( 'vertexshader' ).textContent,
			fragmentShader : document.getElementById( 'fragmentshader' ).textContent,

			blending	: THREE.NormalBlending,
			depthTest	: true,
			transparent	: false
		});
	},
	
	addParticles: function(){
		var data1ParticleCount = Math.floor(data_head.length/3),
		dummy = 0;
		T.geometry 	   = new THREE.Geometry();

		// console.log( data1ParticleCount + ' particles in head array!');

		// for ( var i = 0; i < data1ParticleCount; i+=T.decreaseParticlesBy ) {
		for ( var i = 0; i < data1ParticleCount; i+=1 ) {
			var vector = T.getRandomPointOnparticles(400+Math.random()*T.radius*1.5);
			T.geometry.vertices.push( new THREE.Vertex( vector ) );
			dummy++;
		}
		
		// console.log(dummy + ' particles created.');

		for ( var i = 0; i < T.extraParticles; i++ ) {
			var vector = T.getRandomPointOnparticles(T.radius+Math.random()*T.radius);
			T.geometry.vertices.push( new THREE.Vertex( vector ) );
		}

		T.particles = new THREE.ParticleSystem( T.geometry, T.shaderMaterial );

		var vertices         = T.particles.geometry.vertices,
			values_size      = T.attributes.size.value,
			values_color     = T.attributes.customColor.value,
			values_positiona = T.attributes.custompositiona.value,
			values_positionb = T.attributes.custompositionb.value,
			values_positionc = T.attributes.custompositionc.value,
			values_positiond = T.attributes.custompositiond.value,
			values_positione = T.attributes.custompositione.value,
			values_positionf = T.attributes.custompositionf.value,
			values_positiong = T.attributes.custompositiong.value,
			values_positionh = T.attributes.custompositionh.value,
			values_positioni = T.attributes.custompositioni.value,
			values_positionj = T.attributes.custompositionj.value,
			verticesLength   = vertices.length;
		
		T.baseSize         = [];
		
		// console.log(verticesLength+' vertices counted!');
		
		dummy = 0;
		
		for( var v = 0; v < verticesLength; v++ ) {

			// var index = v*3*T.decreaseParticlesBy;
			var index = v*3;

			T.baseSize[v] = T.particleSizing();

			values_size[ v ]  = T.baseSize[v];
			values_color[ v ] = new THREE.Color( 0xffffff );

			values_color[ v ].setHSV( 0.65, 0.75*( v / vertices.length ), 1.0 );
			// values_color[ v ].setHSV( 0.55, 0.75*( v / vertices.length ), 1.0 );


			if (index > (data1ParticleCount*3)) {
				// for all extra particles
				values_positiona[ v ] = T.getRandomPointOnparticles(400+Math.random()*T.radius*1.5);
				values_positionb[ v ] = T.getRandomPointOnparticles(400+Math.random()*T.radius*1.5);
				values_positionc[ v ] = T.getRandomPointOnparticles(400+Math.random()*T.radius*1.5);
				values_positiond[ v ] = T.getRandomPointOnparticles(400+Math.random()*T.radius*1.5);
				values_positione[ v ] = T.getRandomPointOnparticles(400+Math.random()*T.radius*1.5);
				values_positionf[ v ] = T.getRandomPointOnparticles(400+Math.random()*T.radius*1.5);
				values_positiong[ v ] = T.getRandomPointOnparticles(400+Math.random()*T.radius*1.5);
				values_positionh[ v ] = T.getRandomPointOnparticles(400+Math.random()*T.radius*1.5);
				values_positioni[ v ] = T.getRandomPointOnparticles(400+Math.random()*T.radius*1.5);
				values_positionj[ v ] = T.getRandomPointOnparticles(400+Math.random()*T.radius*1.5);
			}
			else {
				
				dummy++;
				
				var vector = new THREE.Vector3(data_head[index],data_head[index+2],-data_head[index+1]);
				vector.multiplyScalar(120);
				values_positiona[ v ] = vector;

				var vector = new THREE.Vector3(data_hands[index],data_hands[index+2],-data_hands[index+1]);
				vector.multiplyScalar(120);
				values_positionb[ v ] = vector;
				
				var vector = new THREE.Vector3(data_blitz[index],data_blitz[index+2],-data_blitz[index+1]);
				vector.multiplyScalar(120);
				values_positionc[ v ] = vector;
				
				var vector = new THREE.Vector3(data_www[index],data_www[index+2],-data_www[index+1]);
				vector.multiplyScalar(120);
				values_positiond[ v ] = vector;
				
				var vector = new THREE.Vector3(data_graph[index],data_graph[index+2],-data_graph[index+1]);
				vector.multiplyScalar(120);
				values_positione[ v ] = vector;
				
				var vector = new THREE.Vector3(data_clock[index],data_clock[index+2],-data_clock[index+1]);
				vector.multiplyScalar(120);
				values_positionf[ v ] = vector;
				
				var vector = new THREE.Vector3(data_box[index],data_box[index+2],-data_box[index+1]);
				vector.multiplyScalar(120);
				values_positiong[ v ] = vector;
				
				var vector = new THREE.Vector3(data_engage[index],data_engage[index+2],-data_engage[index+1]);
				vector.multiplyScalar(120);
				values_positionh[ v ] = vector;
				
				var vector = new THREE.Vector3(data_market[index],data_market[index+2],-data_market[index+1]);
				vector.multiplyScalar(120);
				values_positioni[ v ] = vector;
				
				var vector = new THREE.Vector3(data_transact[index],data_transact[index+2],-data_transact[index+1]);
				vector.multiplyScalar(120);
				values_positionj[ v ] = vector;
			}
		}
		
		// console.log(dummy+' custom positions created.');

		T.scene.add( T.particles );
	},
	
	getRandomPointOnparticles: function(r) {
		var angle = Math.random() * Math.PI * 2,
			u     = Math.random() * 2 - 1;

		var v = new THREE.Vector3(
			Math.cos(angle) * Math.sqrt(1 - Math.pow(u, 2)) * r,
			Math.sin(angle) * Math.sqrt(1 - Math.pow(u, 2)) * r,
			u * r
		);

		return v;
	},
	
	animate: function(){
		requestAnimationFrame( T.animate );

		T.render();
	},
	
	render: function(){
		T.frame +=0.03;
		// T.uniforms.amplitude.value = Math.sin(T.frame);
		time    = new Date().getTime();
		
		var extra,
			attributesSizeValueLength = T.attributes.size.value.length;
			
		for( var i = 0; i < attributesSizeValueLength; i++ ) {
			T.attributes.size.value[ i ] = T.baseSize[i] + T.baseSize[i] * Math.abs(Math.sin( 0.1 * i + time*0.001 )) + extraSize;
		}
		
		T.attributes.size.needsUpdate = true;
		
		T.uniforms.time.value += T.particleNoiseSpeed;
		
		T.particles.rotation.x += SPEEDX;
		T.particles.rotation.y += SPEEDY;
		T.particles.rotation.z += SPEEDZ;
		
		
		// EventLoop.countDown();
		
		TWEEN.update();
		T.renderer.render( T.scene, T.camera );
	}
};




// ANIMATIONS

T.Animations = {
	
	cameraPosition: "OUT",
	universeMode: true,
	
	bringInParticles: function(){
		// console.log('@bringInParticles');
		T.particles.position.z = -7500;
		var tween = new TWEEN.Tween(T.particles.position)
			.to({z: 0}, 3000)
			.delay(500)
			.easing(TWEEN.Easing.Back.EaseOut);
		tween.start();
	},
	
	showModel: function(model){
		// console.log('@showModel');
		if( SPEEDX !== 0 ){
			T.Animations.stopRotation();
		}
		
		if( model === T.uniforms.direction.value && T.uniforms.amplitude.value === 1 ) {
			return;
		}

		T.Animations.transitionToModel(model);
	},
	
	transitionToModel: function(nr){
		if( T.uniforms.amplitude.value > 0 ){
			// console.log('@transitionToModel - first out');
			var tween = new TWEEN.Tween(T.uniforms.amplitude)
				.to({value: 0}, 2000)
				.easing(TWEEN.Easing.Cubic.EaseInOut)
				.onComplete(function(){ T.Animations.formModel(nr,'fast'); });
				
			tween.start();
		}
		else {
			// console.log('@transitionToModel - directly in');
			T.Animations.formModel(nr,'slow');
		}
	},
	
	formModel: function(nr,mode){
		// console.log('@formModel');
		var tween = new TWEEN.Tween(T.uniforms.amplitude)
			.to({value: 1}, 2000)
			.easing(TWEEN.Easing.Cubic.EaseInOut),
			tween2 = new TWEEN.Tween(T.uniforms.amplitude)
				.to({value: 1}, 3000)
				.easing(TWEEN.Easing.Back.EaseInOut);
			
		T.uniforms.direction.value = nr;
		if( mode === 'slow' ){
			tween2.start();
		}
		else {
			tween.start();
		}
	},
	
	showUniverse: function(callback){
		// console.log('@showUniverse');
		if( T.uniforms.amplitude.value !== 0 ){
			var tween = new TWEEN.Tween(T.uniforms.amplitude)
				.to({value: 0}, 4000)
				.easing(TWEEN.Easing.Back.EaseInOut);
			tween.start();

			T.Animations.startRotation();
		}
	},
	
	rotateX: function(){
		// console.log('@rotateX');
		var rottween = new TWEEN.Tween(T.particles.rotation)
			.to({x: T.particles.rotation.x+Math.PI*2}, 10000)
			.easing(TWEEN.Easing.Elastic.EaseInOut);
		rottween.start();
	},
	
	rotateY: function(){
		// console.log('@rotateY');
		var rottween = new TWEEN.Tween(T.particles.rotation)
			.to({y: T.particles.rotation.y+Math.PI*2}, 10000)
			.easing(TWEEN.Easing.Elastic.EaseInOut);
		rottween.start();
	},
	
	rotateZ: function(){
		// console.log('@rotateZ');
		var rottween = new TWEEN.Tween(T.particles.rotation)
			.to({z: T.particles.rotation.y+Math.PI*2}, 10000)
			.easing(TWEEN.Easing.Elastic.EaseInOut);
		rottween.start();
	},
	
	stopRotation: function(){
		// console.log('@stopRotation');
		SPEEDX = 0;
		SPEEDY = 0;
		SPEEDZ = 0;
		T.Animations.evenOutAxis();
	},
	
	startRotation: function(){
		// console.log('@startRotation');
		SPEEDX = 0.001;
		SPEEDY = 0.001;
		SPEEDZ = 0.001;
	},
	
	evenOutAxis: function(){
		// console.log('@evenOutAxis');
		// return;
		var _toX = Math.round( T.particles.rotation.x/(2*Math.PI) )*2*Math.PI,
			_toY = Math.round( T.particles.rotation.y/(2*Math.PI) )*2*Math.PI,
			_toZ = Math.round( T.particles.rotation.z/(2*Math.PI) )*2*Math.PI,
			rottween = new TWEEN.Tween(T.particles.rotation)
				.to({
					x: _toX,
					y: _toY,
					z: _toZ
				}, 4500)
				.easing(TWEEN.Easing.Cubic.EaseOut);
				
		rottween.start();
	},
	
	spinAround: function(){
		// console.log('@evenOutAxis');
		// return;
		var _toX = (Math.round(T.particles.rotation.x/(2*Math.PI))+1)*(2*Math.PI),
			_toY = (Math.round(T.particles.rotation.y/(2*Math.PI))+1)*(2*Math.PI),
			_toZ = ( Math.round(T.particles.rotation.z/(2*Math.PI)) > T.particles.rotation.z/(2*Math.PI)) ? Math.round(T.particles.rotation.z/(2*Math.PI))*(2*Math.PI) : (Math.round(T.particles.rotation.z/(2*Math.PI))+1)*(2*Math.PI),
			rottween = new TWEEN.Tween(T.particles.rotation)
				.to({
					x: _toX,
					y: _toY,
					z: _toZ
				}, 6000)
				.easing(TWEEN.Easing.Cubic.EaseOut);
				
		rottween.start();
	},
	
	moveCameraIn: function(){
		// console.log('@moveCameraIn');
		T.Animations.cameraPosition = "IN";
		var rottween = new TWEEN.Tween(T.camera.position)
				.to({
					z: 1000
				}, 6000)
				.easing(TWEEN.Easing.Cubic.EaseInOut);
				
		rottween.start();
	},
	
	moveCameraOut: function(){
		// console.log('@moveCameraOut');
		T.Animations.cameraPosition = "OUT";
		var rottween = new TWEEN.Tween(T.camera.position)
				.to({
					z: 4000
				}, 4000)
				.easing(TWEEN.Easing.Cubic.EaseInOut);
				
		rottween.start();
	}
};

var EventLoop = {
	loops: 500,
	looptime: 500,
	model: 0,
	modelMax: 6,
	isPlaying: false,
	active: true,
	
	init: function(){
		
	},
	
	countDown: function(){
		if( EventLoop.active && !EventLoop.isPlaying ){
			EventLoop.loops--;
			// console.log('@countDown: '+EventLoop.loops);
			if( EventLoop.loops === 0 ){
				EventLoop.isPlaying = true;
				EventLoop.loops = 300;
				EventLoop.playSomething();
			}
		}
	},
	
	playSomething: function(){
		// console.log('@playSomething');
		Overlays.show(EventLoop.model);
		EventLoop.model++;
		if( EventLoop.model > EventLoop.modelMax ){
			EventLoop.model = 0;
		}
	}
};

T.A = {
	
	disperse: function(_callback){
		var tween = new TWEEN.Tween(T.uniforms.amplitude)
			.to({value: 0}, 1000)
			.easing(TWEEN.Easing.Cubic.EaseInOut)
			.onComplete(_callback);
		
		tween.start();
	},
	
	model: function(_nr){
		var _next = _nr+1,
			_callback = (_next < 10 ) ? function(){ T.A.disperse(function(){ T.A.model(_next); }) } : function(){ Actions.introIsPlaying = false; EventLoop.loops = 500; EventLoop.isPlaying = false; setTimeout(T.Animations.showUniverse,2000); },
			tween = new TWEEN.Tween(T.uniforms.amplitude)
			.to({value: 1}, 1000)
			.easing(TWEEN.Easing.Cubic.EaseInOut)
			.onComplete(_callback);
		
		_nr = _nr/10;
			
		T.uniforms.direction.value = _nr;
		// console.log('Tween to '+_nr);
			
		tween.start();
	},
	
	start: function(){
		T.A.calmDown();
		if( T.uniforms.amplitude.value === 0 ){
			T.A.model(7);
		}
		else {
			T.A.disperse(function(){ T.A.model(7); });
		}
	},
	
	calmDown: function(){
		SPEEDX = SPEEDY = SPEEDZ = 0;
		T.Animations.evenOutAxis();
	}
};


// INIT

T.init();
// T.Animations.stopRotation();
// T.Animations.showModel(0);


var pusher = new Pusher('167c824c5f47f4ffe1a6', {
    authTransport: 'jsonp',
    authEndpoint: 'http://pusherauth.blocksauth.com/auth/authenticate'
 });

var channel = pusher.subscribe('private-particles');

channel.bind('pusher:subscription_succeeded', function() {
	console.log("OK Subscription is Good");
});

channel.bind('client-controller', function(data) {
  	console.log('An event was triggered with message: ' + data.action + " : "+ data.param);
  	if( typeof Actions[data.action] === 'function' ){
		if( typeof data.param === undefined && typeof data.param === null ){
			data.param = 0;
		}
		Actions[data.action](data.param);
	}
});




var Overlays = {
	
	isShowing: false,
	
	show: function(nr){
		var timeoutDelay = 0;
		
		if( Overlays.isShowing ){ return; }
		Overlays.isShowing = true;
		
		Overlays.hide();
		
		
		if( T.uniforms.amplitude.value > 0 ){
			T.Animations.showUniverse();
			timeoutDelay = 1000;
		}

		// Overlays.t1 = setTimeout(function(){ T.Animations.showModel(nr/10); }, (timeoutDelay));
		// 		Overlays.t2 = setTimeout(T.Animations.showUniverse, (4500+timeoutDelay));
		// 		Overlays.t3 = setTimeout(function(){ $('#overlays').children().eq(nr).addClass('show'); }, (7000+timeoutDelay));
		// 		Overlays.t4 = setTimeout(function(){ Overlays.hide(); Overlays.isShowing = false; EventLoop.isPlaying = false; }, (11000+timeoutDelay));
		
		// Overlays.t1 = setTimeout(function(){ $('#overlays').children().eq(nr).addClass('show'); }, (timeoutDelay));
		Overlays.t2 = setTimeout(function(){ T.Animations.showModel(nr/10); }, (2500+timeoutDelay));
		Overlays.t3 = setTimeout(function(){ T.Animations.showUniverse(); Overlays.isShowing = false; EventLoop.isPlaying = false; }, (8000+timeoutDelay));
		
	},
	
	hide: function(){
		$('#overlays').children().removeClass('show');
		// clearTimeout(Overlays.t1);
		clearTimeout(Overlays.t2);
		clearTimeout(Overlays.t3);
		// clearTimeout(Overlays.t4);
	},
	
	showText: function(nr){
		EventLoop.active = false;
		$('#overlays').children().removeClass('show').eq(nr).addClass('show');
	}
};

var F = {
	
	current: -1,
	
	init: function(){
		this.bindEvents();
	},
	
	bindEvents: function(){
		$('#container').on('webkitAnimationEnd', function(){
			$(this).removeClass('out');
		});
	},
	
	hideThreeJS: function(){
		$('body').addClass('hide_three_js');
	},
	
	showThreeJS: function(){
		if( $('body').hasClass('hide_three_js') ){
			
			$('#container').addClass('out');
			$('#slides').children('.hide').removeClass('active show hide');
			F.current = -1;
			setTimeout(F.delayedshowThreeJS, 100);
		}
	},
	
	delayedshowThreeJS: function(){
		$('#slides').children('.show').removeClass('show');
		$('body').removeClass('hide_three_js');
	},
	
	showSlide: function(nr){
		if(nr > 2 ){ nr = 0; }
		if( nr !== F.current ){
			F.current = nr;
			$('#slides').children().eq(nr).removeClass('active show hide');
			setTimeout(function(){ F.delayedShowSlide(nr); },100);
		}
	},
	
	delayedShowSlide: function(nr){
		F.hideThreeJS();
		$('#container').addClass('out');
		// hide current slide
		$('#slides').children('.show')
			.removeClass('show')
			.addClass('hide');
			
		// slide to show
		$('#slides').children().eq(nr).addClass('active show');
	}
};

var NewSlides = {
	
	on: false,
	active: false,
	lastSlide: -1,	
	
	flash: function(){
		var _flash = $('#newslides').find('.white_flash');
		_flash.removeClass('flash');
		setTimeout(function(){ _flash.addClass('flash'); },100);
	},
	
	show: function(nr){
		var _slides    = $('#newslides').find('.slide'),
			_thisSlide = _slides.eq(nr),
			_flash     = $('#newslides').find('.white_flash');
		
		// if( NewSlides.active ){ return; }
		NewSlides.active = true;
		NewSlides.on = true;
		if( nr === NewSlides.lastSlide ){ return; }
		NewSlides.lastSlide = nr;
			
		// _flash.animate({ opacity : 1 }, 300, function(){
		// 			_slides
		// 				.hide();
		// 			
		// 			_thisSlide
		// 				.show()
		// 				.children('.text')
		// 				.show();
		// 			
		// 			_flash.animate({ opacity : 0 }, 300, function(){
		// 				_flash.delay(1800).animate({ opacity : 1 }, 300, function(){
		// 					_thisSlide.children('.text').hide();
		// 					_flash.animate({ opacity: 0 }, 300);
		// 					NewSlides.active = false;
		// 				});
		// 			});
		// 		});
		
		_flash
		.stop(true)
		.animate({ opacity : 1 }, 300, function(){
			_slides
				.hide();
			
			_thisSlide
				.show()
				.children('.text')
				.show();
		})
		.delay(310)
		.animate({ opacity : 0 }, 300)
		.delay(1500)
		.animate({ opacity : 1 }, 300, function(){
			_thisSlide.children('.text').hide();
			NewSlides.active = false;
		})
		.delay(310)
		.animate({ opacity: 0 }, 300);
	},
	
	hide: function(){
		
		var _slides    = $('#newslides').find('.slide'),
			_flash     = $('#newslides').find('.white_flash');
			
		if( NewSlides.on ){
			_flash.animate({ opacity : 1 }, 300, function(){
				_slides.hide();
				_flash.animate({ opacity : 0 }, 300);
			});
		}
		else {
			_slides.hide();
		}
		
		NewSlides.on = false;
		NewSlides.lastSlide = -1;
	}
};

var Actions = {
	
	introIsPlaying: false,
	
	showSlide: function(nr){
		Overlays.hide();
		Actions.ambientMode(false);
		if( nr < 0 || nr === undefined ){ nr = 0; }
		if( nr > 5 ){ nr = 5; }
		
		// F.showSlide(nr);
		NewSlides.show(nr);
	},
	
	showModel: function(nr){
		F.showThreeJS();
		Overlays.hide();
		Actions.ambientMode(false);
		
		if( typeof Actions.returnToUniverse === 'number' ){ clearTimeout(Actions.returnToUniverse); }
		if( Actions.introIsPlaying ){ return; }
		
		
		if( nr < 0 ){ nr = 0; }
		if( nr > 9 ){ nr = 9; }
		nr = nr/10;
		
		T.Animations.evenOutAxis();
		T.Animations.showModel(nr);
		Actions.returnToUniverse = setTimeout(T.Animations.showUniverse,6000);
	},
	
	showThreeJS: function(){
		Overlays.hide();
		// F.showThreeJS();
		NewSlides.hide();
		Actions.ambientMode(true);
	},
	
	showIntro: function(){
		F.showThreeJS();
		Overlays.hide();
		Overlays.isShowing = false;
		EventLoop.isPlaying = true;
		
		if( Actions.introIsPlaying ){ return; }
		Actions.introIsPlaying = true;
		
		if( typeof Actions.returnToUniverse === 'number' ){ clearTimeout(Actions.returnToUniverse); }
		
		T.A.start();
	},
	
	ambientMode: function(on_off){
		if( on_off ){
			// F.showThreeJS();
			NewSlides.hide();
			EventLoop.active = true;
			EventLoop.loops = 300;
		}
		else {
			EventLoop.active = false;
		}
	}
};

$(function(){
	F.init();
});
